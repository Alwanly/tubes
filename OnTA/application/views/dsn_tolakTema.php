<div class="row">
	<div class="col-md-3">
		<nav class="sidebar">
			<div class="row nav">
				<div class="col-md-12">
					<p id="navhead"><b>Hello, Tika Subagya!</b></p>
				</div>
			</div>
			<a href="<?php echo base_url(); ?>Dosen">
			<div class="row nav1">
				<div class="col-md-12">
					<p id="nav">Dashboard</p>
				</div>
			</div>
			</a>
			<a href="<?php echo base_url(); ?>Dosen/profil">
			<div class="row nav2">
				<div class="col-md-12">
					<p id="nav">Profil Dosen</p>
				</div>
			</div>
			</a>
			<a href="<?php echo base_url(); ?>Dosen/mhsBimbingan">
			<div class="row nav1">
				<div class="col-md-12">
					<p id="nav">Mahasiswa Bimbingan</p>
				</div>
			</div>
			</a>
			
			<div class="row nav2 active">
				<div class="col-md-12">
					<p id="nav">Penyetujuan Tema TA</p>
				</div>
			</div>

			<a href="<?php echo base_url(); ?>Dosen/timelineProgress">
			<div class="row nav1">
				<div class="col-md-12">
					<p id="nav">Timeline Progress</p>
				</div>
			</div>
			</a>
		</nav>
	</div>

	<!-- Content -->
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Detail Tema TA Mahasiswa</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="panel">
					<p id="title"><b>Form Penolakan Tema TA</b></p>
					<form method="POST" action="">
						<table cellpadding="8">
							<tr>
								<td><label>NIM </label></td>
								<td width="450px"> <input type="number" class="form-control" name="nim" value="1202171235" required disabled></td>
							</tr>
							<tr>
								<td><label>Nama Lengkap </label></td>
								<td> <input type="text" class="form-control" name="nama" value="Muhammad Rafi Raharjo" required disabled></td>
							</tr>
							<tr>
								<td><label>Kelas </label></td>
								<td> <input type="text" class="form-control" name="kelas" value="SI-41-02" required disabled></td>
							</tr>
							<tr>
								<td><label>Peminatan </label></td>
								<td> <input type="text" class="form-control" name="peminatan" value="Enterprise System Development" required disabled></td>
							</tr>
							<tr>
								<td><label>Tema TA </label></td>
								<td> <input type="text" class="form-control" name="tema" value="Security Smart Home" required disabled></td>
							</tr>
							<tr>
								<td><label>Abstraksi </label></td>
								<td> <button name="abstraksi" class="btn btn-secondary" id="submit" onclick="">Open File</button></td>
							</tr>
							<tr>
								<td><label>Catatan </label></td>
								<td> <textarea name="catatan" class="form-control"></textarea></td>
							</tr>
							<tr>
								<td></td>
								<td> <input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit"> <button name="cancel" id="submit" class="btn btn-secondary" onclick="window.history.back();">Cancel</button></td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
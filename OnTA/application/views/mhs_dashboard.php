<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, Muhammad Wasis Alyafi!</b></p>
					</div>
				</div>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Mahasiswa/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Mahasiswa</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/requestDosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Request Dosen Pembimbing</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/pengajuanTema">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Pengajuan Tema TA</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/timelineProgress">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Timeline Progress</p>
					</div>
				</div>
				</a>
			</nav>
		</div>	

	<!-- Content -->
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Dashboard</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="box green">
					<br>
					<p id="box">Status Request Dosen</p>
					<br>
					<h3><b>APPROVED</b></h3>
				</div>
				<div class="box yellow">
					<br>
					<p id="box">Status Pengajuan Tema</p>
					<br>
					<h3><b>SUBMITTED</b></h3>
				</div>
				<div class="box grey">
					<br>
					<p id="box">Persentase Progress TA</p>
					<br>
					<h3><b>NOT AVAILABLE</b></h3>
				</div>
			</div>
		</div>
	</div>
</div>
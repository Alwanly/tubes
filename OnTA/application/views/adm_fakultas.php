<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, Mr. Admin!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Admin">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Admin</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunDosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Akun Dosen</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunMahasiswa">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Akun Mahasiswa</p>
					</div>
				</div>
				</a>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Fakultas</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Admin/jurusan">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Jurusan</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/kelas">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Kelas</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/peminatan">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Peminatan</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-9">
				<h4 id="title">List Fakultas</h4>
			</div>
			<div class="col-md-2">
				<a href="<?php echo base_url(); ?>Admin/tambahFakultas"><button name="addFakultas" class="btn btn-primary">Tambah Fakultas</button></a>
			</div>
			<div class="col-md-1"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-11">
				<table class="table">
					<thead class="thead-light">
						<tr>
							<th>No</th>
							<th>Fakultas</th>
							<th>Kode Fakultas</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>Fakultas Ekonomi dan Bisnis</td>
							<td>FEB</td>
							<td><a href="<?php echo base_url(); ?>Admin/editFakultas">Edit</a> / <a href="<?php echo base_url(); ?>Admin/hapusFakultas">Hapus</a></td>
						</tr>
						<tr>
							<td>2</td>
							<td>Fakultas Teknik Elektro</td>
							<td>FTE</td>
							<td><a href="#">Edit</a> / <a href="#">Hapus</a></td>
						</tr>
						<tr>
							<td>3</td>
							<td>Fakultas Rekayasa Industri</td>
							<td>FRI</td>
							<td><a href="#">Edit</a> / <a href="#">Hapus</a></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
</div>
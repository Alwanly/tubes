<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, Muhammad Wasis Alyafi!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Mahasiswa">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Mahasiswa</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/requestDosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Request Dosen Pembimbing</p>
					</div>
				</div>
				</a>
				
				<div class="row nav2 active">
					<div class="col-md-12">
						<p id="nav">Pengajuan Tema TA</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Mahasiswa/timelineProgress">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Timeline Progress</p>
					</div>
				</div>
				</a>
			</nav>
		</div>	

	<!-- Content -->
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Pengajuan Tema TA</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<div class="row">
					<div class="col-md-12">
						<p id="title"><b>Form Pengajuan Tema Tugas Akhir</b></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<form method="POST" action="#">
							<table cellpadding="8">
								<tr>
									<td><label>Dosen Pembimbing </label></td>
									<td width="450px"> <input type="text" class="form-control" name="dosen" value="Tika Subagya Ph.D" required disabled></td>
								</tr>
								<tr>
									<td><label>Tema TA </label></td>
									<td> <input type="text" class="form-control" name="tema" required></td>
								</tr>
								<tr>
									<td><label>Abstraksi </label></td>
									<td> <input type="file" name="abstraksi" required></td>
								</tr>
								<tr>
									<td></td>
									<td align="right"><input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit"> <input type="reset" name="cancel" id="reset" class="btn btn-secondary" value="Cancel"></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
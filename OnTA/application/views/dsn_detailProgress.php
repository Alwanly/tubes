<div class="row">
	<div class="col-md-3">
		<nav class="sidebar">
			<div class="row nav">
				<div class="col-md-12">
					<p id="navhead"><b>Hello, Tika Subagya!</b></p>
				</div>
			</div>
			<a href="<?php echo base_url(); ?>Dosen">
			<div class="row nav1">
				<div class="col-md-12">
					<p id="nav">Dashboard</p>
				</div>
			</div>
			</a>
			<a href="<?php echo base_url(); ?>Dosen/profil">
			<div class="row nav2">
				<div class="col-md-12">
					<p id="nav">Profil Dosen</p>
				</div>
			</div>
			</a>
			<a href="<?php echo base_url(); ?>Dosen/mhsBimbingan">
			<div class="row nav1">
				<div class="col-md-12">
					<p id="nav">Mahasiswa Bimbingan</p>
				</div>
			</div>
			</a>
			<a href="<?php echo base_url(); ?>Dosen/penyetujuanTema">
			<div class="row nav2">
				<div class="col-md-12">
					<p id="nav">Penyetujuan Tema TA</p>
				</div>
			</div>
			</a>
				
			<div class="row nav1 active">
				<div class="col-md-12">
					<p id="nav">Timeline Progress</p>
				</div>
			</div>
			
		</nav>
	</div>

	<!-- Content -->
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Edit Progress</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="panel">
					
					<form method="POST" action="">
						<table cellpadding="8">
							<tr>
								<td><label>Progress ke -</label></td>
								<td width="100px"><input type="number" class="form-control" name="progress" width="50px" value="1" required></td>
								<td width="250px"></td>
							</tr>
							<tr>
								<td><label>Judul Progress</label></td>
								<td colspan="2"><input type="text" class="form-control" name="judul" value="Cover, Abstraksi, Kata Pengantar" required></td>
							</tr>
							<tr>
								<td><label>Due Date</label></td>
								<td colspan="2"><input type="date" class="form-control" name="dueDate" value="08/09/2019" required></td>
							</tr>
							<tr>
								<td></td>
								<td colspan="2"><input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit"> <input type="reset" name="cancel" id="reset" class="btn btn-secondary" value="Cancel"></td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
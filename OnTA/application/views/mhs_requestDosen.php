<!-- Sidebar -->
<div class="row">
	<div class="col-md-3">
		<nav class="sidebar">
			<div class="row nav">
				<div class="col-md-12">
					<p id="navhead"><b>Hello, Muhammad Wasis Alyafi!</b></p>
				</div>
			</div>
			<a href="<?php echo base_url(); ?>Mahasiswa">
			<div class="row nav1">
				<div class="col-md-12">
					<p id="nav">Dashboard</p>
				</div>
			</div>
			</a>
			<a href="<?php echo base_url(); ?>Mahasiswa/profil">
			<div class="row nav2">
				<div class="col-md-12">
					<p id="nav">Profil Mahasiswa</p>
				</div>
			</div>
			</a>
			
			<div class="row nav1 active">
				<div class="col-md-12">
					<p id="nav">Request Dosen Pembimbing</p>
				</div>
			</div>
			
			<a href="<?php echo base_url(); ?>Mahasiswa/pengajuanTema">
			<div class="row nav2">
				<div class="col-md-12">
					<p id="nav">Pengajuan Tema TA</p>
				</div>
			</div>
			</a>
			<a href="<?php echo base_url(); ?>Mahasiswa/timelineProgress">
			<div class="row nav1">
				<div class="col-md-12">
					<p id="nav">Timeline Progress</p>
				</div>
			</div>
			</a>
		</nav>
	</div>	

	<!-- Content -->
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Request Dosen Pembimbing</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<div class="row">
					<div class="col-md-12">
						<p id="title"><b>Form Request Dosen Pembimbing</b></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<form method="POST" action="#">
							<table cellpadding="8">
								<tr>
									<td><label>NIM </label></td>
									<td width="450px"> <input type="text" class="form-control" name="nim" value="1202171234" required disabled></td>
								</tr>
								<tr>
									<td><label>Nama Lengkap </label></td>
									<td> <input type="text" class="form-control" name="nama" value="Muhammad Wasis Alyafi" required disabled></td>
								</tr>
								<tr>
									<td><label>Fakultas </label></td>
									<td> <input type="text" class="form-control" name="fakultas" value="Fakultas Rekayasa Industri" required disabled></td>
								</tr>
								<tr>
									<td><label>Jurusan </label></td>
									<td> <input type="text" class="form-control" name="jurusan" value="S1 Sistem Informasi" required disabled></td>
								</tr>
								<tr>
									<td><label>Kelas </label></td>
									<td> <input type="text" class="form-control" name="nim" value="SI-41-02" required disabled></td>
								</tr>
								<tr>
									<td><label>Peminatan </label></td>
									<td> <input type="text" class="form-control" name="nama" value="Enterprise System Development" required disabled></td>
								</tr>
								<tr>
									<td><label>Pilihan Dosen </label></td>
									<td> <select class="form-control" name="dosen" required>
										<option selected disabled>----- Pilih Dosen -----</option>
										<option>Yatna Subagya Ph.D</option>
										<option>Tika Astriani Ph.D</option>
									</select></td>
								</tr>
								<tr>
									<td></td>
									<td align="right"><input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit"> <input type="reset" name="reset" id="reset" class="btn btn-secondary" value="Cancel"></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
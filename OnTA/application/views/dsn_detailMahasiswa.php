<div class="row">
	<div class="col-md-3">
		<nav class="sidebar">
			<div class="row nav">
				<div class="col-md-12">
					<p id="navhead"><b>Hello, Tika Subagya!</b></p>
				</div>
			</div>
			<a href="<?php echo base_url(); ?>Dosen">
			<div class="row nav1">
				<div class="col-md-12">
					<p id="nav">Dashboard</p>
				</div>
			</div>
			</a>
			<a href="<?php echo base_url(); ?>Dosen/profil">
			<div class="row nav2">
				<div class="col-md-12">
					<p id="nav">Profil Dosen</p>
				</div>
			</div>
			</a>
			
			<div class="row nav1 active">
				<div class="col-md-12">
					<p id="nav">Mahasiswa Bimbingan</p>
				</div>
			</div>
			
			<a href="<?php echo base_url(); ?>Dosen/penyetujuanTema">
			<div class="row nav2">
				<div class="col-md-12">
					<p id="nav">Penyetujuan Tema TA</p>
				</div>
			</div>
			</a>
			<a href="<?php echo base_url(); ?>Dosen/timelineProgress">
			<div class="row nav1">
				<div class="col-md-12">
					<p id="nav">Timeline Progress</p>
				</div>
			</div>
			</a>
		</nav>
	</div>

	<!-- Content -->
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Detail Mahasiswa Bimbingan</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="panel">
					
					<form method="POST" action="">
						<table cellpadding="8">
							<tr>
								<td><label>NIM </label></td>
								<td width="450px"> <input type="number" class="form-control" name="nim" value="1202171234" required disabled></td>
							</tr>
							<tr>
								<td><label>Nama Lengkap </label></td>
								<td> <input type="text" class="form-control" name="nama" value="Muhammad Wasis Alyafi" required disabled></td>
							</tr>
							<tr>
								<td><label>Fakultas </label></td>
								<td> <input type="text" class="form-control" name="fakultas" value="Fakultas Rekayasa Industri" required disabled></td>
							</tr>
							<tr>
								<td><label>Jurusan </label></td>
								<td> <input type="text" class="form-control" name="jurusan" value="S1 Sistem Informasi" required disabled></td>
							</tr>
							<tr>
								<td><label>Kelas </label></td>
								<td> <input type="text" class="form-control" name="kelas" value="SI-41-01" required disabled></td>
							</tr>
							<tr>
								<td><label>Peminatan </label></td>
								<td> <input type="text" class="form-control" name="peminatan" value="Enterprise System Development" required disabled></td>
							</tr>
							<tr>
								<td></td>
								<td><button name="terima" class="btn btn-success" id="submit" onclick="">Terima</button> <button name="tolak" class="btn btn-danger" id="submit" onclick="">Tolak</button></td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
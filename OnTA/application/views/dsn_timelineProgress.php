<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, Tika Subagya!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Dosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Dosen</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/mhsBimbingan">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Mahasiswa Bimbingan</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/penyetujuanTema">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Penyetujuan Tema TA</p>
					</div>
				</div>
				</a>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Timeline Progress</p>
					</div>
				</div>
				
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-9">
				<h4 id="title">Timeline Progress</h4>
			</div>
			<div class="col-md-2">
				<a href="<?php echo base_url(); ?>Dosen/tambahProgress"><button name="addProgress" class="btn btn-primary">Tambah Progress</button></a>
			</div>
			<div class="col-md-1"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="panel-timeline">
					<div class="row">
						<div class="col-md-10">
							<a href="<?php echo base_url(); ?>Dosen/progressMahasiswa"><p id="title"><b>Progress 1</b> - Cover, Abstraksi, Kata Pengantar</p></a>
							<p id="title">Due Date : 8 September 2019</p>
						</div>
						<div class="col-md-2">
							<a href="<?php echo base_url(); ?>Dosen/detailProgress" data-toggle="tooltip" data-placement="bottom" title="Edit"><img class="timeline" src="<?php echo base_url(); ?>assets/icon/edit.png"></a>
							<a href="#" data-toggle="tooltip" data-placement="bottom" title="Delete"><img class="timeline" src="<?php echo base_url(); ?>assets/icon/delete.png"></a>
						</div>
					</div>
				</div>
				<div class="panel-timeline">
					<div class="row">
						<div class="col-md-10">
							<a href="#"><p id="title"><b>Progress 2</b> - BAB 1 Pendahuluan (Latar Belakang, Rumusan Masalah, Batasan Masalah)</p></a>
							<p id="title">Due Date : 15 September 2019</p>
						</div>
						<div class="col-md-2">
							<a href="#" data-toggle="tooltip" data-placement="bottom" title="Edit"><img class="timeline" src="<?php echo base_url(); ?>assets/icon/edit.png"></a>
							<a href="#" data-toggle="tooltip" data-placement="bottom" title="Delete"><img class="timeline" src="<?php echo base_url(); ?>assets/icon/delete.png"></a>
						</div>
					</div>
				</div>
				<div class="panel-timeline">
					<div class="row">
						<div class="col-md-10">
							<a href="#"><p id="title"><b>Progress 3</b> - BAB 1 Pendahuluan (Tujuan dan Manfaat Penelitian, Metode Penelitian, Sistematika Penulisan)</p></a>
							<p id="title">Due Date : 22 September 2019</p>
						</div>
						<div class="col-md-2">
							<a href="#" data-toggle="tooltip" data-placement="bottom" title="Edit"><img class="timeline" src="<?php echo base_url(); ?>assets/icon/edit.png"></a>
							<a href="#" data-toggle="tooltip" data-placement="bottom" title="Delete"><img class="timeline" src="<?php echo base_url(); ?>assets/icon/delete.png"></a>
						</div>
					</div>
				</div>
				<div class="panel-timeline">
					<div class="row">
						<div class="col-md-10">
							<a href="#"><p id="title"><b>Progress 4</b> - BAB 2 Landasan Teori (Teori Umum, Teori Khusus)</p></a>
							<p id="title">Due Date : 6 Oktober 2019</p>
						</div>
						<div class="col-md-2">
							<a href="#" data-toggle="tooltip" data-placement="bottom" title="Edit"><img class="timeline" src="<?php echo base_url(); ?>assets/icon/edit.png"></a>
							<a href="#" data-toggle="tooltip" data-placement="bottom" title="Delete"><img class="timeline" src="<?php echo base_url(); ?>assets/icon/delete.png"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
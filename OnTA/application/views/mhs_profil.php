<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, Muhammad Wasis Alyafi!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Mahasiswa">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				
				<div class="row nav2 active">
					<div class="col-md-12">
						<p id="nav">Profil Mahasiswa</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Mahasiswa/requestDosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Request Dosen Pembimbing</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/pengajuanTema">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Pengajuan Tema TA</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/timelineProgress">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Timeline Progress</p>
					</div>
				</div>
				</a>
			</nav>
		</div>	

	<!-- Content -->
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Profil Mahasiswa</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<div class="row">
					<div class="col-md-4">
						<img class="profile" src="<?php echo base_url(); ?>assets/icon/profile.png">
					</div>
					<div class="col-md-8">
						<form method="POST" action="#">
							<table cellpadding="8">
								<tr>
									<td><label>NIM </label></td>
									<td width="450px"> <input type="number" class="form-control" name="nim" required></td>
								</tr>
								<tr>
									<td><label>Nama Lengkap </label></td>
									<td> <input type="text" class="form-control" name="nama" required></td>
								</tr>
								<tr>
									<td><label>Fakultas </label></td>
									<td> <select class="form-control" name="fakultas" required>
										<option value="" selected disabled>----- Pilih Fakultas -----</option>
										<option value="FEB">Fakultas Ekonomi dan Bisnis</option>
										<option value="FTE">Fakultas Teknik Elektro</option>
										<option value="FRI">Fakultas Rekayasa Industri</option>
									</select></td>
								</tr>
								<tr>
									<td><label>Jurusan </label></td>
									<td> <select class="form-control" name="jurusan" required>
										<option value="" selected disabled>----- Pilih Jurusan -----</option>
										<option value="S1MBTI">S1 Manajemen Bisnis Telekomunikasi dan Informatika</option>
										<option value="S1TE">S1 Teknik Elektro</option>
										<option value="S1SI">S1 Sistem Informasi</option>
									</select></td>
								</tr>
								<tr>
									<td><label>Kelas </label></td>
									<td> <select class="form-control" name="kelas" required>
										<option value="" selected disabled>----- Pilih Kelas -----</option>
										<option value="MBTI-41-01">MBTI-41-01</option>
										<option value="TE-41-02">TE-41-02</option>
										<option value="SI-41-03">SI-41-03</option>
									</select></td>
								</tr>
								<tr>
									<td><label>Peminatan </label></td>
									<td> <select class="form-control" name="peminatan" required>
										<option value="" selected disabled>----- Pilih Peminatan -----</option>
										<option value="Mandat">Manajemen Data</option>
										<option value="Robotik">Robotik</option>
										<option value="ESD">Enterprise System Development</option>
									</select></td>
								</tr>
								<tr>
									<td><label>Email </label></td>
									<td> <input type="email" class="form-control" name="email" required></td>
								</tr>
								<tr>
									<td><label>Username </label></td>
									<td> <input type="text" class="form-control" name="username" required></td>
								</tr>
								<tr>
									<td><label>Password </label></td>
									<td> <input type="password" class="form-control" name="password" required></td>
								</tr>
								<tr>
									<td></td>
									<td><input type="submit" name="submit" id="submit" class="btn btn-success" value="Save"></td>
								</tr>
							</table>
						</form> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
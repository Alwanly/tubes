<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, Muhammad Wasis Alyafi!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Mahasiswa">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Mahasiswa</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/requestDosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Request Dosen Pembimbing</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/pengajuanTema">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Pengajuan Tema TA</p>
					</div>
				</div>
				</a>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Timeline Progress</p>
					</div>
				</div>
				
			</nav>
		</div>	

	<!-- Content -->
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Timeline Progress</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<a href="<?php echo base_url(); ?>Mahasiswa/detailProgress">
				<div class="panel-timeline">
					<div class="row">
						<div class="col-md-12">
							<p id="title"><b>Progress 1</b> - Cover, Abstraksi, Kata Pengantar</p>
							<p id="title">Due Date : 8 September 2019</p>
						</div>
					</div>
				</div>
				</a>
				<a href="#">
				<div class="panel-timeline">
					<div class="row">
						<div class="col-md-12">
							<p id="title"><b>Progress 2</b> - BAB 1 Pendahuluan (Latar Belakang, Rumusan Masalah, Batasan Masalah)</p>
							<p id="title">Due Date : 15 September 2019</p>
						</div>
					</div>
				</div>
				</a>
				<a href="#">
				<div class="panel-timeline">
					<div class="row">
						<div class="col-md-12">
							<p id="title"><b>Progress 3</b> - BAB 1 Pendahuluan (Tujuan dan Manfaat Penelitian, Metode Penelitian, Sistematika Penulisan)</p>
							<p id="title">Due Date : 22 September 2019</p>
						</div>
					</div>
				</div>
				</a>
				<a href="#">
				<div class="panel-timeline">
					<div class="row">
						<div class="col-md-12">
							<p id="title"><b>Progress 4</b> - BAB 2 Landasan Teori (Teori Umum, Teori Khusus)</p>
							<p id="title">Due Date : 6 Oktober 2019</p>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
	</div>
</div>
<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, Muhammad Wasis Alyafi!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Mahasiswa">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Mahasiswa</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/requestDosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Request Dosen Pembimbing</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/pengajuanTema">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Pengajuan Tema TA</p>
					</div>
				</div>
				</a>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Timeline Progress</p>
					</div>
				</div>
				
			</nav>
		</div>	

	<!-- Content -->
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Detail Progress</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="panel">
					<p id="title"><b>Progress 1</b> - BAB 1 Pendahuluan (Latar Belakang, Rumusan Masalah, Batasan Masalah)</p>
					<p id="title">Due Date : 15 September 2019</p>
					<br>
					<form method="POST" action="">
						<table cellpadding="8">
							<tr>
								<td width="100px"><label>Progress</label></td>
								<td><input type="file" name="progress" required></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit"> <input type="reset" name="cancel" id="reset" class="btn btn-secondary" value="Cancel"></td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
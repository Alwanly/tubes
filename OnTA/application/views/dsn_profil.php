<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, Tika Subagya!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Dosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				
				<div class="row nav2 active">
					<div class="col-md-12">
						<p id="nav">Profil Dosen</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Dosen/mhsBimbingan">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Mahasiswa Bimbingan</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/penyetujuanTema">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Penyetujuan Tema TA</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/timelineProgress">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Timeline Progress</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Profil Dosen</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<div class="row">
					<div class="col-md-4">
						<img class="profile" src="<?php echo base_url(); ?>assets/icon/profile.png">
					</div>
					<div class="col-md-8">
						<form method="POST" action="#">
							<table cellpadding="8">
								<tr>
									<td><label>NIP </label></td>
									<td width="450px"> <input type="number" class="form-control" name="nip" required></td>
								</tr>
								<tr>
									<td><label>Nama Lengkap </label></td>
									<td> <input type="text" class="form-control" name="nama" required></td>
								</tr>
								<tr>
									<td><label>Fakultas </label></td>
									<td> <select class="form-control" name="fakultas" required>
										<option value="" selected disabled>----- Pilih Fakultas -----</option>
										<option value="FEB">Fakultas Ekonomi dan Bisnis</option>
										<option value="FTE">Fakultas Teknik Elektro</option>
										<option value="FRI">Fakultas Rekayasa Industri</option>
									</select></td>
								</tr>
								<tr>
									<td><label>Peminatan </label></td>
									<td> <select class="form-control" name="peminatan" required>
										<option value="" selected disabled>----- Pilih Peminatan -----</option>
										<option value="Mandat">Manajemen Data</option>
										<option value="Robotik">Robotik</option>
										<option value="ESD">Enterprise System Development</option>
									</select></td>
								</tr>
								<tr>
									<td><label>Email </label></td>
									<td> <input type="email" class="form-control" name="email" required></td>
								</tr>
								<tr>
									<td><label>Username </label></td>
									<td> <input type="text" class="form-control" name="username" required></td>
								</tr>
								<tr>
									<td><label>Password </label></td>
									<td> <input type="password" class="form-control" name="password" required></td>
								</tr>
								<tr>
									<td></td>
									<td><input type="submit" name="submit" id="submit" class="btn btn-success" value="Save"></td>
								</tr>
							</table>
						</form> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
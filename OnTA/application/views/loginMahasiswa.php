<!DOCTYPE html>
<html lang="en">
<head>
	<title>OnTA - Mahasiswa Login Page</title>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" />
	<script src="<?php echo base_url(); ?>assets/jquery/jquery-3.4.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/login1.css" />
</head>
<body>
<div class="d-md-flex h-md-100 align-items-center">

	<div class="col-md-12 p-0 h-md-100">
	    <div class="align-items-center bg-mahasiswa h-100 justify-content-center">
	    	<div class="centered">
	        	<div class="panelMhs">
	        		<center>
	        		<h2>Login Mahasiswa</h2><br>
	        		<table cellpadding="8">
	        			<tr>
	        				<td><label>Username</label></td>
	        				<td><input type="text" name="username" class="form-control"></td>
	        			</tr>
	        			<tr>
	        				<td><label>Password</label></td>
	        				<td><input type="password" name="password" class="form-control"></td>
	        			</tr>
	        			<tr>
	        				<td colspan="2" align="center"><br><a href="<?php echo base_url(); ?>Login/register"><button name="register" class="btn btn-light register">Register</button></a> <a href="<?php echo base_url(); ?>Login/loginMahasiswa"><button name="submit" class="btn btn-success login">Login</button></a></td>
	        			</tr>
	        		</table>
	        		</center>
	        	</div>
	        </div>
	    </div>
	</div>

</div>
</body>
</html>
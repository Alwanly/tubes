<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, Tika Subagya!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Dosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Dosen</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/mhsBimbingan">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Mahasiswa Bimbingan</p>
					</div>
				</div>
				</a>
				
				<div class="row nav2 active">
					<div class="col-md-12">
						<p id="nav">Penyetujuan Tema TA</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Dosen/timelineProgress">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Timeline Progress</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Penyetujuan Tema Tugas Akhir</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-11">
				<table class="table">
					<thead class="thead-light">
						<tr>
							<th>NIM</th>
							<th>Nama</th>
							<th>Kelas</th>
							<th>Status</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1202171235</td>
							<td>Muhammad Rafi Raharjo</td>
							<td>SI-41-02</td>
							<td>Submitted</td>
							<td><a href="<?php echo base_url(); ?>Dosen/detailTema">Detail</a></td>
						</tr>
						<tr>
							<td>1202171237</td>
							<td>Yatna Subagya</td>
							<td>SI-41-04</td>
							<td>Approved</td>
							<td><a href="#">Detail</a></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
</div>
<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, Mr. Admin!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Admin">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Admin</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunDosen">
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Akun Dosen</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunMahasiswa">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Akun Mahasiswa</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/fakultas">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Fakultas</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/jurusan">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Jurusan</p>
					</div>
				</div>
				</a>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Kelas</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Admin/peminatan">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Peminatan</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Tambah Kelas</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<div class="row">
					<div class="col-md-12">
						<p id="title"><b>Form Tambah Kelas Baru</b></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<form method="POST" action="#">
							<table cellpadding="8">
								<tr>
									<td><label>Kelas </label></td>
									<td width="450px"> <input type="text" class="form-control" name="kelas" required></td>
								</tr>
								<tr>
									<td><label>Jurusan </label></td>
									<td> <select class="form-control" name="jurusan" required>
										<option value="" selected disabled>----- Pilih Jurusan -----</option>
										<option value="1">S1 Manajemen Bisnis Telekomunikasi dan Informatika</option>
										<option value="2">S1 Teknik Elektro</option>
										<option value="3">S1 Sistem Informasi</option>
									</select></td>
								</tr>
								<tr>
									<td></td>
									<td><input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit"> <input type="reset" name="cancel" id="reset" class="btn btn-secondary" value="Cancel"></td>
								</tr>
							</table>
						</form> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
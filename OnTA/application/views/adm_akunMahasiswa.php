<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, Mr. Admin!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Admin">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Admin</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunDosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Akun Dosen</p>
					</div>
				</div>
				</a>
				
				<div class="row nav2 active">
					<div class="col-md-12">
						<p id="nav">Akun Mahasiswa</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Admin/fakultas">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Fakultas</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/jurusan">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Jurusan</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/kelas">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Kelas</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/peminatan">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Peminatan</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Mahasiswa Bimbingan</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-11">
				<table class="table">
					<thead class="thead-light">
						<tr>
							<th>NIM</th>
							<th>Nama</th>
							<th>Fakultas</th>
							<th>Jurusan</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1202171234</td>
							<td>Muhammad Wasis Alyafi</td>
							<td>FRI</td>
							<td>S1 Sistem Informasi</td>
							<td><a href="<?php echo base_url(); ?>Admin/hapusMahasiswa">Hapus</a></td>
						</tr>
						<tr>
							<td>1202171235</td>
							<td>Muhammad Rafi Raharjo</td>
							<td>FRI</td>
							<td>S1 Sistem Informasi</td>
							<td><a href="#">Hapus</a></td>
						</tr>
						<tr>
							<td>1202171236</td>
							<td>Tika Astriani</td>
							<td>FRI</td>
							<td>S1 Sistem Informasi</td>
							<td><a href="#">Hapus</a></td>
						</tr>
						<tr>
							<td>1202171237</td>
							<td>Yatna Subagya</td>
							<td>FRI</td>
							<td>S1 Sistem Informasi</td>
							<td><a href="#">Hapus</a></td>
						</tr>
						<tr>
							<td>1202171238</td>
							<td>Alwan Alyafi</td>
							<td>FRI</td>
							<td>S1 Sistem Informasi</td>
							<td><a href="#">Hapus</a></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
</div>
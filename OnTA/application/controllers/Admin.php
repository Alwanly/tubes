<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('AdminModel');
	}

	public function index()
	{

		$this->load->view('adm_header');
		$this->load->view('adm_dashboard');
		$this->load->view('adm_footer');
	}

	public function profil()
	{
		$data['profile'] = $this->AdminModel->getAdmin()->row();
		// var_dump($data);
		$this->load->view('adm_header');
		$this->load->view('adm_profil',$data);
		$this->load->view('adm_footer');
	}

	public function akunDosen()
	{
		$data['dosen'] = $this->AdminModel->getDosen();				
		$this->load->view('adm_header');
		$this->load->view('adm_akunDosen',$data);
		$this->load->view('adm_footer');
	}

	public function tambahDosen()
	{
		$data['fks'] = $this->AdminModel->getFakultas();	
		$data['pmnts'] = $this->AdminModel->getPeminatan();	
		//  var_dump($data);
		$this->load->view('adm_header');
		$this->load->view('adm_tambahDosen',$data);
		$this->load->view('adm_footer');
	}


	public function akunMahasiswa()
	{
		$this->load->view('adm_header');
		$this->load->view('adm_akunMahasiswa');
		$this->load->view('adm_footer');
	}

	public function fakultas()
	{
		$this->load->view('adm_header');
		$this->load->view('adm_fakultas');
		$this->load->view('adm_footer');
	}

	public function tambahFakultas()
	{
		$this->load->view('adm_header');
		$this->load->view('adm_tambahFakultas');
		$this->load->view('adm_footer');
	}

	public function editFakultas()
	{
		$this->load->view('adm_header');
		$this->load->view('adm_editFakultas');
		$this->load->view('adm_footer');
	}

	public function jurusan()
	{
		$this->load->view('adm_header');
		$this->load->view('adm_jurusan');
		$this->load->view('adm_footer');
	}

	public function tambahJurusan()
	{
		$this->load->view('adm_header');
		$this->load->view('adm_tambahJurusan');
		$this->load->view('adm_footer');
	}

	public function editJurusan()
	{
		$this->load->view('adm_header');
		$this->load->view('adm_editJurusan');
		$this->load->view('adm_footer');
	}

	public function kelas()
	{
		$this->load->view('adm_header');
		$this->load->view('adm_kelas');
		$this->load->view('adm_footer');
	}

	public function tambahKelas()
	{
		$this->load->view('adm_header');
		$this->load->view('adm_tambahKelas');
		$this->load->view('adm_footer');
	}

	public function editKelas()
	{
		$this->load->view('adm_header');
		$this->load->view('adm_editKelas');
		$this->load->view('adm_footer');
	}

	public function peminatan()
	{
		$this->load->view('adm_header');
		$this->load->view('adm_peminatan');
		$this->load->view('adm_footer');
	}

	public function tambahPeminatan()
	{
		$this->load->view('adm_header');
		$this->load->view('adm_tambahPeminatan');
		$this->load->view('adm_footer');
	}

	public function editPeminatan()
	{
		$this->load->view('adm_header');
		$this->load->view('adm_editPeminatan');
		$this->load->view('adm_footer');
	}
	
	public function editAdmin()
	{
		$data['id'] = $this->input->post('id');
		$data['NIK'] = $this->input->post('nik');
		$data['nama'] = $this->input->post('nama');
		$data['username'] = $this->input->post('username');
		$data['password'] = $this->input->post('password');
		$this->AdminModel->updateAdmin($data);
		$this->profil();
	}
		public function CreateDosen(){
		$data['NIP'] = 	$this->input->post('nip');
		$data['nama_dsn'] = 	$this->input->post('nama');
		$data['pmnt'] = 	$this->input->post('peminatan');
		$data['email'] = 	$this->input->post('email');
		$data['username'] = 	$this->input->post('username');
		$data['password'] = 	$this->input->post('password');
		$this->AdminModel->CreateDosen($data);
		$this->akunDosen();
		
	}
		public function hapusDosen($data){
			// $data = $this->input->get('id');
			var_dump($data);
			$this->db->where('id',$data);
			$this->db->delete('tb_dosen');
			$this->akunDosen();
			
		}
		
	
}
?>
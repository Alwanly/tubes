<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {
	
	public function index()
	{
		$this->load->view('mhs_header');
		$this->load->view('mhs_dashboard');
		$this->load->view('mhs_footer');
	}

	public function profil()
	{
		$this->load->view('mhs_header');
		$this->load->view('mhs_profil');
		$this->load->view('mhs_footer');
	}

	public function requestDosen()
	{
		$this->load->view('mhs_header');
		$this->load->view('mhs_requestDosen');
		$this->load->view('mhs_footer');
	}

	public function pengajuanTema()
	{
		$this->load->view('mhs_header');
		$this->load->view('mhs_pengajuanTema');
		$this->load->view('mhs_footer');
	}

	public function timelineProgress()
	{
		$this->load->view('mhs_header');
		$this->load->view('mhs_timelineProgress');
		$this->load->view('mhs_footer');
	}

	public function detailProgress()
	{
		$this->load->view('mhs_header');
		$this->load->view('mhs_detailProgress');
		$this->load->view('mhs_footer');
	}
}
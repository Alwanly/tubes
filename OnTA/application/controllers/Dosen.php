<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {

	public function index()
	{
		$this->load->view('dsn_header');
		$this->load->view('dsn_dashboard');
		$this->load->view('dsn_footer');
	}

	public function profil()
	{
		$this->load->view('dsn_header');
		$this->load->view('dsn_profil');
		$this->load->view('dsn_footer');
	}

	public function mhsBimbingan()
	{
		$this->load->view('dsn_header');
		$this->load->view('dsn_mhsBimbingan');
		$this->load->view('dsn_footer');
	}

	public function detailMahasiswa()
	{
		$this->load->view('dsn_header');
		$this->load->view('dsn_detailMahasiswa');
		$this->load->view('dsn_footer');
	}

	public function penyetujuanTema()
	{
		$this->load->view('dsn_header');
		$this->load->view('dsn_penyetujuanTema');
		$this->load->view('dsn_footer');
	}

	public function detailTema()
	{
		$this->load->view('dsn_header');
		$this->load->view('dsn_detailTema');
		$this->load->view('dsn_footer');
	}

	public function tolakTema()
	{
		$this->load->view('dsn_header');
		$this->load->view('dsn_tolakTema');
		$this->load->view('dsn_footer');
	}

	public function timelineProgress()
	{
		$this->load->view('dsn_header');
		$this->load->view('dsn_timelineProgress');
		$this->load->view('dsn_footer');
	}

	public function tambahProgress()
	{
		$this->load->view('dsn_header');
		$this->load->view('dsn_tambahProgress');
		$this->load->view('dsn_footer');
	}

	public function detailProgress()
	{
		$this->load->view('dsn_header');
		$this->load->view('dsn_detailProgress');
		$this->load->view('dsn_footer');
	}

	public function progressMahasiswa()
	{
		$this->load->view('dsn_header');
		$this->load->view('dsn_progressMahasiswa');
		$this->load->view('dsn_footer');
	}

	public function detailProgressMhs()
	{
		$this->load->view('dsn_header');
		$this->load->view('dsn_detailProgressMhs');
		$this->load->view('dsn_footer');
	}

	public function tolakProgress()
	{
		$this->load->view('dsn_header');
		$this->load->view('dsn_tolakProgress');
		$this->load->view('dsn_footer');
	}
}
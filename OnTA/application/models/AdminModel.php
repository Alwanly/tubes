<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminModel extends CI_Model {

	public function getAdmin()
	{
		$query= $this->db->get_where('tb_admin',array('id'=>1));
		return $query;
	}
	public function updateAdmin($data){				
		$array = array(
			'nama'=>$data['nama'],
			'NIK'=>$data['NIK'],
			'username'=>$data['username'],
			'password'=>$data['password']);			
			$this->db->set($array);
			$this->db->where('id',$data['id']);
			$this->db->update('tb_admin');			
	}
	public function getDosen()
	{
		$query= $this->db->select('id,NIP,nama_dsn,nama_fks,nama_jur');
		$query= $this->db->from('tb_dosen,tb_fakultas,tb_jurusan');		
		$query= $this->db->get();
		return $query;
	}
	public function createDosen ($data)
	{
		$array= array(
			'NIP' =>$data['NIP'],
			'nama_dsn'=>$data['nama_dsn'],
			'email'=>$data['email'],
			'username'=>$data['username'],
			'password'=>$data['password'],
			'id_pmntn' =>$data['pmnt'],
			'kouta'=>30
		);
		$this->db->insert('tb_dosen',$array);
	}
	public function getFakultas()
	{
		$query = $this->db->get('tb_fakultas');
		return $query;
	}
	public function getJurusan()
	{
		$query = $this->db->get('tb_jurusan');
		return $query;
	}
	public function getPeminatan()
	{
		$query = $this->db->get('tb_peminatan');
		return $query;
	}


}
